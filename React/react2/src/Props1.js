import React, { Component } from 'react'

export default class Props1 extends Component {
  render() {
    return (
      <div>
        <h1>Hello {this.props.name}!!</h1>
      </div>
    )
  }
}

