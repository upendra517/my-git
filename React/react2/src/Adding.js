import React, { useState } from 'react';

const Adding = () => {
    const [items, updateItems] = useState([]);
    const [input, updateInput] = useState({
        input1: "",
        input2: ""
    });

    const changeHandler = (e) => {
        const { name, value } = e.target;
        updateInput(prevInput => ({
            ...prevInput,
            [name]: value
        }));
    };

    const addItems = () => {
        const newItem = {
            name: input.input1,
            amount: parseInt(input.input2, 10)
        };
        updateItems(prevItems => [...prevItems, newItem]);
        updateInput({
            input1: "",
            input2: ""
        });
    };

    const totalAmount = items.reduce((total, item) => total + item.amount, 0);

    return (
        <div className='container'>
            <br /><br />
            <input 
                type="text" 
                name="input1" 
                value={input.input1} 
                onChange={changeHandler} 
                placeholder="Name"
            />
            <input 
                type="text" 
                name="input2" 
                value={input.input2}  
                onChange={changeHandler} 
                placeholder="Amount"
            /><br />
            <button onClick={addItems}>Submit</button><br /><br />

            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map((item, index) => (
                        <tr key={index}>
                            <td>{item.name}</td>
                            <td>{item.amount}</td>
                        </tr>
                    ))}
                </tbody>
                <tfoot>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><strong>{totalAmount}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    );
}

export default Adding;
