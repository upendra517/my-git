import React, { useState } from 'react'

const State2 = () => {
  const[name,updateName] = useState("upendra")
  const[input,updateInput] =useState("")
  
  const changeHandler = (e) => {
    updateInput(e.target.value)
  }
  const changeName = () => {
    updateName(input)
  }
  return (
    <div>
      <h1>Name: {name}</h1>
      <input type="text" name="input" value={input} onChange={changeHandler} />
      <button onClick={changeName}>Change</button>
    </div>
  )
}

export default State2
