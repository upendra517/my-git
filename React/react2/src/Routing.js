import React from 'react'
import Navbar from './Navbar'
import { Route, Routes,BrowserRouter } from 'react-router-dom'
import Home from './Home'
import Login from './Login'
import Register from './Register'
import Products from './Products'

function Error() {
  return(
    <>
    <h1>Page not Found</h1>
    <p>404 error...</p>
    </>
  )
}

const Routing = () => {
  return (
    <div>
        <BrowserRouter>
        <Navbar></Navbar>
        <Routes>
            <Route path='home' element={<Home></Home>}/>
            <Route path='login' element={<Login></Login>}>
            <Route path='products' element={<Products/>}/>
            </Route>
            <Route path='register' element={<Register></Register>}/>
            <Route path='*' element={<Error/>}/>
        </Routes>
        </BrowserRouter>
    </div>
  )
}

export default Routing
