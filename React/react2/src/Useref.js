import React, { useRef, useState } from 'react'

const Useref = () => {
    let number = useRef(10)
    const[count,updateCount]=useState(10)
  return (
    <div className='d-flex justify-content-center'>
        <h1>Number: {number.current}</h1>
      <button onClick={()=>{
        updateCount(count+10)
        number.current = number.current + 10;
        console.log(number.current)
      }}>Click</button>
    </div>
  )
}

export default Useref
   