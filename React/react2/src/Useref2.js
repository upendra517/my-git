import React, { useRef, useState } from 'react'

const Useref2 = () => {
    let nameref = useRef()
    const[name,updateName] = useState('upendra')
  return (
    <div>
      <h1>Name: {name}</h1>
      <input type='text' ref={nameref} />

      <button onClick={()=>{
        updateName(nameref.current.value)
      }}>Click</button>
    </div>
  )
}

export default Useref2
