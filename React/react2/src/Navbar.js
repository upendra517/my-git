import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <div>
      <ol>
        <li className='mr-5'><Link to='/home'>Home</Link></li>
        <li className='mr-5'><Link to='/login'>Login</Link></li>
        <li className='mr-5'><Link to='/register'>Register</Link></li>
        <li className='mr-5'><a href='/register'>Register with anchor tag</a></li>
      </ol>
    </div>
  );
}

export default Navbar;
