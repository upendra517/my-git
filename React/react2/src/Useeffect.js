import React, { useEffect, useState } from 'react'

const Useeffect = () => {
    const[number,updateNumber] = useState(0)

    useEffect(()=>{
        console.log("useeffect")
    },[number])

    const addNumber = () => {
        updateNumber(number+1)
    }
  return (
    <div className='d-flex justify-content-center'>
        <h1>Number: {number}</h1><br/><br/>
        <button onClick={addNumber}>Click</button>
    </div>
  )   
}
   
export default Useeffect
