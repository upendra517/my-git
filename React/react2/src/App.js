import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Props1 from './Props1'
import State1 from './State1';
import State2 from './State2';
import Adding from './Adding';
import Useeffect from './Useeffect';
import Routing from './Routing';
import Useref from './Useref';
import Useref2 from './Useref2';

function App() {
  return (
    <div>
      {/* <Props1 name="upendra"></Props1> */}
      {/* <State1></State1> */}
      {/* <State2></State2> */}
      {/* <Adding></Adding> */}
      {/* <Useeffect></Useeffect> */}
      {/* <Routing></Routing> */}
      {/* <Useref></Useref> */}
      <Useref2></Useref2>
    </div>
  );
}

export default App;
