import React from 'react'
import Products from './Products'
import { Outlet } from 'react-router'
import { Link } from 'react-router-dom'

const Login = () => {
  return (
    <div>
      <button><Link to="products" >Products</Link></button>
      <h1>Login component</h1>
      <Outlet></Outlet>
    </div>
  )
}
   
export default Login
