import React, { Component } from 'react'

export default class State1 extends Component {
  state = {
    name : "upendra",
    setName : ""
  }

  changeHandler = (e) => {
    this.setState({setName:e.target.value})
  }
  changeName =() => {
    this.setState({name:this.state.setName})
  }
  render() {
    return (
      <div>
      <h1>Name: {this.state.name}</h1>
      <input type='text' name="name" value={this.state.setName} onChange={this.changeHandler} />  
      <br/>
      <button onClick={this.changeName}>Change</button>   
       </div>
    )
  }
}
