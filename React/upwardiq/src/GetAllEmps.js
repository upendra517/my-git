import React, { useEffect, useState } from 'react';
import axios from 'axios';

const GetAllEmps = () => {
  // Create a state variable to hold employee data
  const [employees, setEmployees] = useState([]);

  useEffect(() => {
    fetchEmployees();
  }, []);

  // Fetch employee data from the backend
  const fetchEmployees = async () => {
    try {
      const response = await axios.get('http://localhost:8086/getall');
      setEmployees(response.data); // Store the fetched data in the state
    } catch (error) {
      console.error('Error fetching employees:', error);
    }
  };

  return (
    <div>
      <h1>Employee List</h1>
      <table border="1" style={{ width: '100%', borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Department</th>
            <th>Email</th>
            <th>Salary</th>
          </tr>
        </thead>
        <tbody>
          {/* Map through the employees array to display each employee in a row */}
          {employees.map((employee) => (
            <tr key={employee.id}>
              <td>{employee.id}</td>
              <td>{employee.name}</td>
              <td>{employee.dept}</td>
              <td>{employee.email}</td>
              <td>{employee.salary}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default GetAllEmps;
