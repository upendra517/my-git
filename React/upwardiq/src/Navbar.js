import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav style={{ padding: '10px', backgroundColor: '#333', color: 'white' }}>
      <ul style={{ listStyleType: 'none', display: 'flex', gap: '20px' }}>
        <li>
          <Link to="/login" style={{ color: 'white', textDecoration: 'none' }}>Login</Link>
        </li>
        <li>
          <Link to="/addEmployee" style={{ color: 'white', textDecoration: 'none' }}>Add Employee</Link>
        </li>
        <li>
          <Link to="/getAllEmps" style={{ color: 'white', textDecoration: 'none' }}>Get All Employees</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navbar;
