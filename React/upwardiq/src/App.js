import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Login from './Login';
import Home from './Home';
import AddEmployee from './AddEmployee';
import GetAllEmps from './GetAllEmps';
import Navbar from './Navbar'; // Import Navbar component

const App = () => {
  return (
    <Router>
      <div>
        {/* Include the Navbar component */}
        <Navbar />
        
        {/* Define the Routes for the pages */}
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/home" element={<Home />} />
          <Route path="/addEmployee" element={<AddEmployee></AddEmployee>} />
          <Route path="/getAllEmps" element={<GetAllEmps />} />
        </Routes>
      </div>
    </Router>
  );
};

export default App;
