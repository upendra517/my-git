import React, { useContext } from 'react'
import { context } from './App'

const Usecontext = () => {
    let data = useContext(context)
    return(
        <>
        <h1>{data.name}</h1>
        <h2>{data.gender}</h2>
        </>
    )
}
export default Usecontext

