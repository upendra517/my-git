import React, { useEffect, useState } from 'react'
import axios  from 'axios'

function Axioss(){

    const [products,updateProducts] = useState([])

    useEffect(
        ()=>{
            getProducts()
        },[]
    )

    async function getProducts(){
        let res = await axios.get('https://fakestoreapi.com/products')
        console.log(res.data)
        updateProducts(res.data)
        
    }

    return(
        <>
        <ul>
            {
              products.map((p)=>{
                return(
                    <li key={p.id}>
                        <h1>{p.title}</h1>
                        <h3>{p.price}</h3>
                    </li>
                )
              })
            }
        </ul>
        </>
    )

}
export default Axioss