
import React, { useReducer } from 'react'

function reducer(state,action){
    if(action.type === "add"){
        return state + action.payload
    }
}

const Usereducer1 = () => {
    const[number,dispatch] =useReducer(reducer,0)
  return (
    <div className='container'>
      <h1>{number}</h1>
      <input type='text' id="number"/><br/><br/>
      <button onClick={()=>{
        let p = parseInt(document.getElementById("number").value) 
        dispatch({type:"add",payload:p})
      }}>submit</button><br/>
    </div>
  )
}

export default Usereducer1
