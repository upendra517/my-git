import React, { useState } from 'react'

const Form = () => {
   const[data,updateData] = useState({
        name:" ",
        age:" ",
        salary:" "
})

   const {name,age,salary} = data;

   const changeData = (e) =>{
    updateData({...data,[e.target.name]:e.target.value})
   }

   const submitHandler = (e) => {
    e.preventDefault();
    console.log(data)

   }


  return (
    <div className='container mt-5'>
      <form onSubmit={submitHandler}>
      <input type='text' name="name" value={name} onChange={changeData}/><br/><br/>
      <input type='text'name="age" value={age} onChange={changeData}/><br/><br/>
      <input type='text'name="salary" value={salary} onChange={changeData}/><br/><br/>
      <input type='submit'name="submit" /><br/>
      </form>
    </div>
  )
}

export default Form
