
import './App.css';
import Counter from './Counter';
import Axioss from './Axioss';
import Usereducer1 from './Usereducer1';
import React, { createContext } from 'react';
import Usecontext from './Usecontext';
import Routing from './Routing';
import GetAllEmployees from './GetAllEmployees';
import AddEmployee from './AddEmployee';
import Form from './Form'

export const context = createContext()

function App() {

  // let p = {
  //   name : "upendra",
  //   gender: "male"
  // }

  return (
    <React.StrictMode>

      {/* <Routing></Routing> */}

      {/* <context.Provider value={p}>
      <Usecontext></Usecontext>
    </context.Provider> */}
      {/* <Counter></Counter> */}
      {/* <Axioss></Axioss> */}
      {/* <Usereducer></Usereducer> */}

      {/* <GetAllEmployees></GetAllEmployees> */}
      {/* <AddEmployee></AddEmployee> */}
      {/* <Usereducer1></Usereducer1> */}
      <Form></Form>
    </React.StrictMode>
  );
}

export default App;
