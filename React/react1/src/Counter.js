import React, { useState } from 'react'

function Counter(){
  
    const [count,updateCount] = useState(0)

    return(
        <>
        <center>
            <button onClick={()=>{
                if(count>0){
                    updateCount(count-1)
                }
            }}>-</button>
        <h1>{count}</h1>
        <button onClick={()=>{
            if(count<10){
                updateCount(count+1)
            }
        }}>+</button>
        </center>
        </>
    )
}
export default Counter