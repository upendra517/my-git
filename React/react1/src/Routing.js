import React from "react";
import Login, { Products } from './Login';
import Register from './Register';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Navbar from './Navbar';
import Home from './Home';

const Routing = () => {
    return (
        <>
            <BrowserRouter>
                <Navbar />
                <Routes>
                    <Route path='/' exact element={<Home />} />
                    <Route path='login' element={<Login />}>
                        <Route path='products' element={<Products />} />
                    </Route>
                    <Route path='register' element={<Register />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default Routing