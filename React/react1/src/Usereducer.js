
import React, { useReducer } from 'react'

function changeCount(state,action) {

    console.log(state,action.type)
    switch(action.type){
        case 'inc' : return state + 1;
        case 'dec' : return state - 1;
        case 'reset': return 0;
        default : return state;
    }
    
}

export const Usereducer = () => {
    const [count, updateCount] = useReducer(changeCount, 0)
    return (
        <>
            <center className='container mt-4 d-flex'>
                <button className='m-2 btn btn-primary' onClick={() => {
                    updateCount({ type: 'dec'})
                }}>-</button>
                <h1>Counter: {count}</h1>
                <button className='m-2 btn btn-primary' onClick={() => {
                    updateCount({ type: 'inc'})
                }}>+</button>
                <button className='m-2 btn btn-danger' onClick={() => {
                    updateCount({ type: 'reset'})
                }}>Reset</button>
            </center>
        </>
    )
}
