import axios from 'axios'
import React, { useState } from 'react'

const AddEmployee = () => {

    const [employee, setEmployee] = useState({
        city: " ",
        name: " ",
        salary: " "
    })

    const { name, salary, city } = employee;

    const changeEmployee = (e) => {
        setEmployee({ ...employee, [e.target.name]: e.target.value })
    }

    const submitHandler = async (e) => {
        e.preventDefault()
        let res = await axios.post('http://localhost:8086/addEmployee', employee)
        console.log(res.data)
        console.log(res.data.salary)
        console.log(res.data.city)
    }


    return (
        <div className='container'>
            <form onSubmit={submitHandler}>
                <input type="text" name="name" value={name} onChange={changeEmployee} /><br />
                <input type="text" name="salary" value={salary} onChange={changeEmployee} /><br />
                <input type="text" name="city" value={city} onChange={changeEmployee} /><br />
                <input type='submit' name="Submit" />
            </form>

        </div>
    )
}

export default AddEmployee
