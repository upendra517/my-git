import axios from 'axios'
import React, { useEffect, useState } from 'react'

const GetAllEmployees = () => {

    const [employee ,setEmployee] = useState([])

    useEffect(()=>{
        getEmployees()
    },[])

   async function getEmployees(){
        let emp = await axios.get('http://localhost:8086/getAllEmployees')
        console.log(emp.data);
        setEmployee(emp.data)
    }

    return (
        <div className='container'>
            <h1 className='text-center'>All Employees</h1>
            <table className='table table-striped table-bordered'>
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Name</td>
                        <td>Salary</td>
                        <td>City</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        employee.map((emp)=>
                            (
                                <tr key={emp.id}>
                                    <td>{emp.id}</td>
                                    <td>{emp.name}</td>
                                    <td>{emp.salary}</td>
                                    <td>{emp.city}</td>
                                </tr>
                            )
                        )
                    }
                </tbody>
            </table>
        </div>
    )
}

export default GetAllEmployees
