import React from 'react'
import { Outlet } from 'react-router-dom'
import { Link } from 'react-router-dom'

const Login = () => {
    return (
        <>

            <div>
                <button><Link to='products'>products</Link></button>
            </div>

            <h1>Login component</h1>
            <Outlet></Outlet>
        </>
    )
}

export function Products() {
    return (
        <>
            <h1>products component</h1>
        </>
    )
}

export default Login
