import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = ()=>{
    return(
        <>
        <h1 className='d-flex'>
            <h2 className='m-2'><Link to='/'>Home</Link></h2>
            <h2 className='m-2'><Link to='login'>Login</Link></h2>
            <h2 className='m-2'><Link to='register'>Register</Link></h2>
        </h1>
        </>
    )
}
export default Navbar