package arrayprograms;

public class MaxConsicutiveOnes {

	public static void main(String[] args) {

		int arr[] = { 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1 };

		int max = 0;
		for (int i = 0; i < arr.length; i++) {
			int count = 0;
			for (int j = i; j < arr.length; j++) {
				if (arr[j] == 1) {
					count++;
				} else {
					break;
				}
			}
			if (max < count) {
				max = count;
			}
		}
		System.out.println(max);
	}
}
