package arrayprograms;

public class ZeroesToEnd {

	public static void main(String[] args) {

		int arr[] = { 1, 0, 2, 0, 0, 3, 4, 0, 5 };

		int k = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != 0) {
				int temp = arr[k];
				arr[k] = arr[i];
				arr[i] = temp;
				k++;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			if (i < arr.length - 1) {
				System.out.print(arr[i] + " ,");
			} else {
				System.out.print(arr[i]);
			}
		}
	}
}
