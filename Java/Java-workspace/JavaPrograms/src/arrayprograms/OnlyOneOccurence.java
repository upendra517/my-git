package arrayprograms;

public class OnlyOneOccurence {

	public static void main(String[] args) {
		int arr[] = { 1, 2, 3, 4, 3, 2, 1 };

		for (int i = 0; i < arr.length; i++) {
			int count = 0;
			for (int j = 0; j < arr.length; j++) {
				if (i != j && arr[i] == arr[j]) {
					count++;
					break;
				}
			}
			if (count == 0) {
				System.out.println(arr[i]);
				break;
			}
		}
//--------------------------------------------------------------------------		
//HashMap<Integer,Integer> hm = new HashMap<>();
//		
//		for(int i=0;i<arr.length;i++){
//			int n = arr[i];
//			if(hm.containsKey(n)){
//				hm.put(n,hm.get(n)+1);
//			}
//			else {
//				hm.put(n, 1);
//			}
//		}
//		for(Entry<Integer,Integer> key : hm.entrySet() ){
//			if(key.getValue() == 1){
//				System.out.println(key.getKey());
//			}
//		}

	}
}