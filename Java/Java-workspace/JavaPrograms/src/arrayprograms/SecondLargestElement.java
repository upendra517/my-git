package arrayprograms;

import java.util.Arrays;

public class SecondLargestElement {

	public static void main(String[] args) {
		int arr[] = { 5, 3, 2, 1, 4, 3, 4, 5, 5 };
		Arrays.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		for (int i = arr.length - 2; i >= 0; i--) {
			if (arr[i] < arr[arr.length - 1]) {
				System.out.println(arr[i]);
				break;
			}

		}

	}
}