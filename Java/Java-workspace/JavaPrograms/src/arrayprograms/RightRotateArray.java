package arrayprograms;

public class RightRotateArray {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5, 6, 7 };
		int n = 6;

		int[] result = rightRotateArray(arr, n);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + " ");
		}
		System.out.println();

	}

	public static int[] rightRotateArray(int arr[], int n) {
		int l = arr.length;
		int[] arr1 = new int[n];
		for (int i = 0; i < n; i++) {
			arr1[i] = arr[l + i - n];
		}
		int k = l - n - 1;
		for (int i = l - 1; i >= n; i--) {
			arr[i] = arr[k];
			k--;
		}

		for (int i = 0; i < n; i++) {
			arr[i] = arr1[i];
		}

		return arr;
	}

}
