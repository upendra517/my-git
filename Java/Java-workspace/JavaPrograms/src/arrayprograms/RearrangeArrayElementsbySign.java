package arrayprograms;

public class RearrangeArrayElementsbySign {

	public static void main(String[] args) {

		int arr[] = { 3, 1, -2, -5, 2, -4 };
		int[] arr2 = new int[arr.length];
		int k = 0;
		int l = 1;
		for (int i = 0; i < arr.length; i++) {
			int n = arr[i];
			if (n < 0) {
				arr2[l] = n;
				l += 2;
			} else {
				arr2[k] = n;
				k += 2;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr2[i] + " ");
		}
	}     
}
