package arrayprograms;

import java.util.ArrayList;

public class Shifting0sAtEnd {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 0, 5, 8, 0, 0, 4, 5, 6, 0, 9, 0, 8, 0 };

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

		System.out.println();

		int result[] = shiftZeroesToLastIndex(arr);

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}

	}

	public static int[] shiftZeroesToLastIndex(int[] arr) {

		ArrayList<Integer> al = new ArrayList<>();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 0) {
				al.add(arr[i]);
				arr[i] = 0;
			}
		}

		for (int i = 0; i < al.size(); i++) {
			arr[i] = al.get(i);
		}

		return arr;
	}
}
