package arrayprograms;

public class LeftRotateArray {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int n = 8;

		int[] result = leftRotateArray(arr, n);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + " ");
		}
		System.out.println();

	}

	public static int[] leftRotateArray(int arr[], int n) {

		int[] arr1 = new int[n];
		for (int i = 0; i < n; i++) {
			arr1[i] = arr[i];
		}

		for (int i = n; i < arr.length; i++) {
			arr[i - n] = arr[i];
		}

		for (int i = 0; i < n; i++) {
			arr[arr.length - n + i] = arr1[i];
		}

		return arr;
	}

}
