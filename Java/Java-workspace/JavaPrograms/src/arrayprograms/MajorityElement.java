package arrayprograms;

import java.util.*;
public class MajorityElement {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,2,3,4,3,2,2,2,3,3};
          System.out.println(majorityElement(arr));

	}
	 public static List<Integer> majorityElement(int[] nums) {
	        int l = nums.length/3;

	        ArrayList<Integer> al = new ArrayList<>();
	        
	        HashMap<Integer,Integer> hm = new HashMap<>();
	        
	        for(int i=0;i<nums.length;i++){
	            int n = nums[i];
	            if(hm.containsKey(n)){
	               hm.put(n,hm.get(n)+1); 
	            } else{
	                hm.put(n,1);
	            }
	        }
	        for(Map.Entry<Integer,Integer> entry : hm.entrySet()){
	            if(entry.getValue()>l){
	                al.add(entry.getKey());
	            }
	        }
	        return al;
	    }
}
