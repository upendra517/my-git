package arrayprograms1;

public class RightRotate {

	public static void main(String[] args) {
		int arr[] = {1,2,3,4,5};
		int k=2;
		
		int[] result = rightRotate(arr,k);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}

	}
	
	public static int[] rightRotate(int[] arr,int k){
		
		int[] arr1 = new int[k];
		for(int i=0;i<arr1.length;i++){
			arr1[i] = arr[arr.length-1-i];
		}
		
		for(int i=arr.length-1;i>=k;i--){
			arr[i] = arr[i-k];
		}
		
		for(int i=0;i<arr1.length;i++){
			arr[i] = arr1[arr1.length-1-i];
		}
		return arr;
	}

}
