package arrayprograms1;

import java.util.*;
public class SecondLargest {

	public static void main(String[] args) {
		
		int arr[] = {1,4,5,3,6,2,6};
		
		Arrays.sort(arr);
		
		int max = arr[arr.length-1];
		
		for(int i=arr.length-2;i>=0;i--){
			if(arr[i]<max){
				System.out.println(arr[i]);
				break;
			}
		}

	}

}
