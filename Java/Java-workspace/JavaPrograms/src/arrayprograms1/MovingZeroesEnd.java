package arrayprograms1;

public class MovingZeroesEnd {

	public static void main(String[] args) {
		
		int arr[] = {0,0,0,9,9,9,0,1,2,0,0,0,0,0,3,0,0,4,5,0};
		
		for(int i=0;i<arr.length;i++){
			int n=i+1;
			if(arr[i] == 0){
				while(n<arr.length){
					if(arr[n] != 0){
						int temp = arr[i];
						arr[i] = arr[n];
						arr[n] = temp;
						break;
					} else {
						n++;
					}
				}
			}
		}
		
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}

	}

}
