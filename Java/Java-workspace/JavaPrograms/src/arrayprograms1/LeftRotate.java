package arrayprograms1;

public class LeftRotate {

	public static void main(String[] args) {
		int arr[] = {1,2,3,4,5};
		int k=2;
		
		int[] result = leftRotate(arr,k);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}

	}
	
	public static int[] leftRotate(int[] arr, int k){
		int[] arr1 = new int[k];
		
		for(int i=0;i<k;i++){
			arr1[i] = arr[i];
		}
		
		for(int i=0;i<=k;i++){
			arr[i] = arr[2+i];
		}
		
		for(int i=0;i<arr1.length;i++){
			arr[arr.length-1-i] = arr1[k-1];
			k--;
		}
		return arr;
	}

}
