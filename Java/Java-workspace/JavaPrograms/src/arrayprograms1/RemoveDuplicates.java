package arrayprograms1;

import java.util.HashSet;

public class RemoveDuplicates {

	public static void main(String[] args) {
		
		int arr[] = {1,3,2,1,3,2,4,4,3,4};
		
		HashSet<Integer> hs = new HashSet<>();
		
		for(int i=0;i<arr.length;i++){
			hs.add(arr[i]);
		}
		
		System.out.println(hs);

	}

}
