package arrayprograms1;

public class LongestSubArrayWithSumK {

	public static void main(String[] args) {
		
		int arr[] = {1,2,3,1,1,1,1,4,2,3};
		int k=4;
		int max = 0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]==k && max<1){
				max = 1;
			}
			int sum =arr[i];
			int count = 1;
			for(int j=i+1;j<arr.length;j++){
			    sum = sum + arr[j];
				count++;
				if(sum == k){
					if(max<count){
						max =count;
						break;
					}
				}
				if(sum > k){
					break;
				}
			}
		}
		System.out.println(max);
	}
}
