package arrayprograms1;

public class AddingNumberOddPositions {

	public static void main(String[] args) {
		
		int n =1234;
        int position = 2;
        int sum =0;
        
        while(n>0){
            int number = n%10;
            if(position % 2 != 0){
                sum = sum + number;
            }
            position++;
            n = n/10;
        }
        
        System.out.println(sum);

	}

}
