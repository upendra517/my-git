package pattren;

public class ReverseTriangle {

	public static void main(String[] args) {
		
		for(int i=1;i<6;i++){
			for(int j=1;j<=i;j++){
				System.out.print("  ");
			}
			for(int k=i;k<5;k++){
				System.out.print("* ");
			}
			for(int l=i;l<6;l++){
				System.out.print("* ");
			}
			System.out.println();
		}
	}
}
