package matrixprograms;

public class MatrixMultiplication {

	public static void main(String[] args) {
		
	int arr1[][] = {{1,1,1},{2,2,2},{3,3,3}};
		
		int arr2[][] = {{1,1,1},{2,2,2},{3,3,3}};
		
		int[][] arr = new int[3][3];
		
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr1[i].length;j++){
				for(int k=0;k<arr[i].length;k++){
					arr[i][j] = arr[i][j] + arr1[i][k]*arr2[k][j];
				}
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}
