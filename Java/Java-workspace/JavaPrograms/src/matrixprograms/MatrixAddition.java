package matrixprograms;

public class MatrixAddition {

	public static void main(String[] args) {
		
		int arr1[][] = {{1,3,4},{2,3,4},{3,4,5}};
		
		int arr2[][] = {{1,3,4},{2,3,4},{1,2,4}};
		
		int[][] arr = new int[3][3];
		
		for(int i=0;i<arr1.length;i++){
			for(int j=0;j<arr1[i].length;j++){
				arr[i][j] = arr1[i][j]+arr2[i][j];
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}

}
