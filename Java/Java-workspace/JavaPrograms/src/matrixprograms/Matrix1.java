package matrixprograms;

import java.util.Scanner;

public class Matrix1 {

	public static void main(String[] args) {
		
		int[][] arr = new int[3][3];
		
		Scanner scan = new Scanner(System.in);
		System.out.println("enter elements");
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				arr[i][j]=scan.nextInt();
			}
		}
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				System.out.print(arr[j][i]+" ");
			}
			System.out.println();
		}

	}

}
