package basicprograms;

public class ArmstrongNumber {

	public static void main(String[] args) {
		
		int n =1530;
		int temp =n;
		int sum =0;
		
		while(n>0){
			int n1 = n % 10;
			sum = sum + (int)Math.pow(n1, 3);
			n = n/10;
		}
		if(sum == temp){
			System.out.println(temp+" is Armstrong Number");
		} else {
			System.out.println(temp+" is not Armstrong Number");
		}
	}

}
