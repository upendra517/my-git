package basicprograms;

public class PalindromeNumber {

	public static void main(String[] args) {
		
		int n = 54945;
		int temp = n;
		
		int sum =0;
		
		while(n>0){
			int n1 = n % 10;
			sum = (sum*10)+n1;
			n = n /10;
		}
		if(sum == temp){
			System.out.println(temp +" is palindrome");
		}else {
			System.out.println(temp+" is not palindrome");
		}
	}
}
