package leetcode;

public class TwoSum {

	public static void main(String[] args) {
		
		int arr[] = {3,2,3};
		int t=6;
		
		int[] result = twoSum(arr,t);
		for(int i=0;i<result.length;i++){
			System.out.print(result[i]+" ");
		}
	}
	 public static int[] twoSum(int[] nums, int target) {
		 
	        int arr2[] = new int[2];
	        
	        for(int i=0;i<nums.length-1;i++){ 
	            for(int j=i+1;j<nums.length;j++){
	                int sum =0;
	            sum = nums[i] + nums[j];
	            if(sum == target){
	                arr2[0] = i;
	                arr2[1] = j;
	                break;
	            }
	            }
	        }
	        return arr2;
	 }
}
