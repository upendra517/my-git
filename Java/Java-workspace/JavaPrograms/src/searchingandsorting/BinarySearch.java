package searchingandsorting;

import java.util.Arrays;

public class BinarySearch {

	public static void main(String[] args) {
		
		int arr[] = {3,1,5,8,6,2,9,7};
		int key = 7;
		
		
		int result = binarySearch(key,arr);
		if(result != -1){
			System.out.println(key +" at index " +result);
		} else {
			System.out.println("element not found");
		}
	}
	public static int binarySearch(int key,int[] arr){
		Arrays.sort(arr);
		
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		
		int l = 0;
		int r = arr.length-1;
		
		while(l<=r){
			int m = (l+r)/2;
			if(key > arr[r]){
				return -1;
			}
			else if(arr[m] == key){
				return m;
			}
			else if(key > arr[m]){
				l = m+1;
			} else {
				r = m-1;
			}
		}
		return -1;
	}

}
