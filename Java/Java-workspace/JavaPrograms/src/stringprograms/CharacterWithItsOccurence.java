package stringprograms;

import java.util.*;

public class CharacterWithItsOccurence {
	public static void main(String[] args) {
		String str = "aabbcc";
		String str1 = "";
		HashMap<Character, Integer> hm = new HashMap<>();
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);

			if (hm.containsKey(ch)) {
				hm.put(ch, hm.get(ch) + 1);
			} else {
				hm.put(ch, 1);
			}
			str1 = str1 + String.valueOf(hm.get(ch));
			//str = str.replaceFirst(String.valueOf(ch), String.valueOf(hm.get(ch)));
		}
		System.out.println(str1);
	}

}
