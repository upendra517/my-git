package stringprograms;

import java.util.HashMap;

public class WordPattern {

	public static void main(String[] args) {
		String str1 = "abba";
		String str2 = "dog cat cat dog";

		String[] arr = str2.split(" ");

		HashMap<Character, String> hm = new HashMap<>();
		int count = 0;
		for (int i = 0; i < str1.length(); i++) {
			char ch1 = str1.charAt(i);
			String s = arr[i];

			if (hm.containsKey(ch1) && hm.containsValue(s)) {
				hm.put(ch1, s);
			} else if (!hm.containsKey(ch1) && !hm.containsValue(s)) {
				hm.put(ch1, s);
			} else {
				count++;
			}
		}
		System.out.println(count);
		System.out.println(hm);

		if (count == 0) {
			System.out.println("Word Pattern-True");
		} else {
			System.out.println("False");
		}
	}
}
