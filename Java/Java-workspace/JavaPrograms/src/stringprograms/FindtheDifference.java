package stringprograms;

public class FindtheDifference {

	public static void main(String[] args) {
		long sum1 = 0;
		long sum2 = 0;
		String str1 = "abcd";
		String str2 = "abcde";
		for (int i = 0; i < str1.length(); i++) {
			int n = str1.charAt(i);
			sum1 = sum1 + n;
		}
		for (int i = 0; i < str2.length(); i++) {
			int n = str2.charAt(i);
			sum2 = sum2 + n;
		}
		char ch = (char) (sum2 - sum1);
		System.out.println(ch);

	}

}
