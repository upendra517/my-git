package stringprograms;

public class ReverseOnlyLetters {

	public static void main(String[] args) {
		
		String str = "ab-c?*d-";
		System.out.println(str);
		char[] arr = str.toCharArray();
		
			int s = 0;
			int e = arr.length-1;
			
			while(s<e){
				if(!Character.isLetter(arr[s])){
					s++;
				}
				else if(!Character.isLetter(arr[e])){
					e--;
				} else {
					char temp = arr[s];
					arr[s] = arr[e];
					arr[e] = temp;
					s++;
					e--;
				}
			}
		System.out.println(new String(arr));
	}
}
