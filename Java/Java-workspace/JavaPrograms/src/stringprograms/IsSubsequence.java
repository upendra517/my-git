package stringprograms;

public class IsSubsequence {

	public static void main(String[] args) {

		  String str1 = "akbhncy";
			String str2 = "abc";
			int count=0;
			int n=0;
			for(int i=0;i<str2.length();i++){
			    char ch = str2.charAt(i);
			    while(n<str1.length()){
			        if(str1.charAt(n)==ch){
			            count++;
			            n++;
			            break;
			        }
			        n++;
			    }
			}
			if(count == str2.length()){
			    System.out.println(count+" yes");
			} else {
			    System.out.println(count+" noo");
			}
	}

}
