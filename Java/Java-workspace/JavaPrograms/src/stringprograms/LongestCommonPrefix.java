package stringprograms;

import java.util.Arrays;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		   
	      String[] arr = {"flower","flight","flow"};
	      
	      Arrays.sort(arr);
	      String str = arr[0];
	      String str2 = arr[arr.length-1];
	      String prefix = "";
	      for(int i=0;i<str.length();i++){
	          if(str.charAt(i)==str2.charAt(i)){
	              prefix = prefix + String.valueOf(str.charAt(i));
	          }
	      }
	      System.out.println(prefix);
	}

}
