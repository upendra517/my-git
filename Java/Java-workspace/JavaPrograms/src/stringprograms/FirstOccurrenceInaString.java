package stringprograms;

public class FirstOccurrenceInaString {

	public static void main(String[] args) {
		String str1 = "sadbutnotsad";
		String str2 = "not";
		int l = str2.length();
		int count = 0;
		int i;
		for (i = 0; i <= str1.length() - l; i++) {
			String str = str1.substring(i, l + i);
			if (str.equals(str2)) {
				System.out.println(str + " " + i);
				count++;
				break;
			}
		}
		if (count == 0) {
			System.out.println(-1);
		} else {
			System.out.println(i);
		}

	}

}
