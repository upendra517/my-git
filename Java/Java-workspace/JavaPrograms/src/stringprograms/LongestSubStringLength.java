package stringprograms;

import java.util.ArrayList;

public class LongestSubStringLength {

	public static void main(String[] args) {

		String str = "abcaeaklmn";
		int max = 1;
		int l=0;
		ArrayList<Character> al = new ArrayList<>();

		for (int i = 0; i < str.length(); i++) {
			al.clear();
			al.add(str.charAt(i));
			for(int j=i+1;j<str.length();j++){
				char ch = str.charAt(j);
				if(!al.contains(ch)){
					al.add(ch);
				} else {
					break;
				}
				l =al.size();
			}
			if(max<l){
				max = l;
				System.out.println(al);
			}
		}
		System.out.println(max);
	}
}