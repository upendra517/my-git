package stringprograms;

import java.util.ArrayList;

public class ReverseVowels {

	public static void main(String[] args) {

		String str = "leetcode";
		System.out.println(str);
		char[] arr = str.toCharArray();

		ArrayList<Character> al = new ArrayList<>();

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u') {
				al.add(arr[i]);
			}
		}
		int n = 0;
		for (int i = arr.length - 1; i >= 0; i--) {
			if (arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u') {
				arr[i] = al.get(n);
				n++;
			}
		}
		System.out.println(new String(arr));
	}

}
