package stringprograms;

public class EqualsMethod {

	public static void main(String[] args) {
		String str1 = new String("upendra");
		String str2 = new String("upendra");
		
		StringBuffer sb1 = new StringBuffer("upendra");
		StringBuffer sb2 = new StringBuffer("upendra");
		
		System.out.println(str1.equals(str2));//comparing content
		System.out.println(sb1.equals(sb2));//comparing address
	}

}
