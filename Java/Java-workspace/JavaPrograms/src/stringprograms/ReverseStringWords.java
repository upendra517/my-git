package stringprograms;

public class ReverseStringWords {

	public static void main(String[] args) {
		
		String str = "Good Morning Upendra !!!";
		
		System.out.println(reverseStringWords(str));

	}
	
	public static String reverseStringWords(String str){
		
		String[] arr = str.split(" ");
		
		for(int i=0;i<arr.length;i++){
			arr[i] = reverse(arr[i]);
		}
		
		String str1 = String.join(" ", arr);
		
		return str1;
	}
	
	public static String reverse(String str){
		String str1 = "";
		for(int i=str.length()-1;i>=0;i--){
			str1 = str1 + str.charAt(i);
		}
		return str1;
	}

}
