package stringprograms;

import java.util.HashMap;

public class IsomorphicStrings {

	public static void main(String[] args) {
		
		 String str1 = "egg";
	      String str2 = "bab";
	      
	      HashMap<Character,Character> hm = new HashMap<>();
	      int count = 0;
	      for(int i=0;i<str1.length();i++){
	          char ch1 = str1.charAt(i);
	          char ch2 = str2.charAt(i);
	          
	          if(hm.containsKey(ch1) && hm.containsValue(ch2)){
	              hm.put(ch1,ch2);
	          } else if(!hm.containsKey(ch1) && !hm.containsValue(ch2)){
	              hm.put(ch1,ch2);
	          }else{
	              count++;
	          }
	      }
	      System.out.println(count);
	            System.out.println(hm);

	      if(count == 0){
	          System.out.println("strings are isomorphic");
	      } else{
	          System.out.println("nott");
	      }
	}

}
