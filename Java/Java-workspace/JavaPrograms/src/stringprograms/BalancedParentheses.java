package stringprograms;

import java.util.Stack;

public class BalancedParentheses {

	public static void main(String[] args) {

		String str = "{[]}";
		
		System.out.println(balancedParentheses(str));
	}
	
	//return "\"Total: " + sum + ". Days above double average: " + days + ".\"";----**************
	
	public static boolean balancedParentheses(String str){
		
		Stack<Character> st = new Stack<>();
		
		for(int i=0;i<str.length();i++){
			char ch = str.charAt(i);
			
			if(ch == '{'){
				st.push('}');
				
			}else if(ch == '['){
				st.push(']');
				
			}else if(ch == '('){
				st.push(')');
				
			}
			else {
				if(st.empty() || st.pop() != ch){
					return false;
				}
			}
		}
		return st.isEmpty();
	}
}

//important stack pop ------------------------------------------------------------------
//Stack<Character> stack = new Stack<>();
//
//stack.push('q');
//stack.push('a');
//stack.push('b');
//stack.push('c');
//
//// Pop elements until the stack is empty
//while (!stack.isEmpty()) {
//    System.out.println(stack.pop());
//}
