package stringprograms;

public class WordOccursAsaPrefix {

	public static void main(String[] args) {
		String str1 = "i love eating burger";
		String str2 = "burg";

		String[] arr = str1.split(" ");
		for (int i = 0; i < arr.length; i++) {
			String str = arr[i];
			int count = 0;
			if (str.length() >= str2.length()) {
				for (int j = 0; j < str2.length(); j++) {
					if (str.charAt(j) == str2.charAt(j)) {
						count++;
					}
				}
			}
			if (count == str2.length()) {
				System.out.println(count);
				break;
			}
		}

	}

}
