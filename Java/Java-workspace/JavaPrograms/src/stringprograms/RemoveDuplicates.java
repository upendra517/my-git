package stringprograms;

public class RemoveDuplicates {

	public static void main(String[] args) {

		      String str = "aabbccbbaa";
		      char ch2 = ' ';
		      char[] arr = str.toCharArray();
		      
		      for(int i=0;i<arr.length;i++){
		          char ch = arr[i];
		         for(int j=0;j<arr.length;j++){
		             char ch1 = arr[j];
		             if(i!=j && ch == ch1){
		                 arr[j] = ch2;
		             }
		         }
		      }
		      String str1 = new String(arr).replace(" ","");
		      System.out.println(str1);
		       
		    }
}
