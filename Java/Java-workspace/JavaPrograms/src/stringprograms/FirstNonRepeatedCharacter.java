package stringprograms;

public class FirstNonRepeatedCharacter {

	public static void main(String[] args) {
		String str = "aabccdbe";
		
		for(int i=0;i<str.length();i++){
	         int count =0;
	         for(int j=0;j<str.length();j++){
	             if(i!=j && str.charAt(i) == str.charAt(j)){
	                 count++;
	         }
	         }
	         if(count == 0){
	             System.out.println(str.charAt(i));
	             break;
	         }
	     }

//		for (int i = 0; i < str.length(); i++) {
//			boolean b = true;
//			for (int j = 0; j < str.length(); j++) {
//				if (i != j && str.charAt(i) == str.charAt(j)) {
//					b = false;
//				}
//			}
//			if (b) {
//				System.out.println(str.charAt(i));
//				break;
//			}
//		}
	}
}
