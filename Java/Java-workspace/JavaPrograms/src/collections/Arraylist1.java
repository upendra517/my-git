package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Arraylist1 {

	public static void main(String[] args) {
		
		ArrayList<Integer> al =new ArrayList<>();
		
		al.add(1);
		al.add(2);
		al.add(5);
		al.add(3);
		al.add(9);
		al.add(4);
		al.add(5);
		al.add(3);
		
		System.out.println(al);
		
		Collections.sort(al);
		System.out.println(al);
		
		for(int i=al.size()-1;i>=0;i--){
			System.out.print(al.get(i)+" ");
		}
		System.out.println();
		
		al.set(2, 10);
		
		al.remove(2);
		
		al.add(2,10);
		
		System.out.println(al.indexOf(3));
		System.out.println(al.lastIndexOf(3));
		
		Iterator<Integer> itr = al.iterator();
		
		while(itr.hasNext()){
			System.out.print(itr.next()+" ");
		}
		
		Object[] arr = al.toArray();
	}
}
