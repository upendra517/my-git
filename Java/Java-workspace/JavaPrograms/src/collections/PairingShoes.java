package collections;

import java.util.*;

public class PairingShoes {

	public static void main(String[] args) {
		int[][] shoes = { { 0, 20 }, { 1, 22 }, { 0, 22 }, { 1, 20 }, { 0, 21 }, { 1, 24 } };

		Map<Integer, Integer> left = new HashMap<>();
		Map<Integer, Integer> right = new HashMap<>();

		for (int[] arr : shoes) {
			int size = arr[1];
			int type = arr[0];

			if (type == 0) {
				left.put(size, 0);
			} else {
				right.put(size, 1);
			}
		}

		int count = 0;
		for (int key : left.keySet()) {
			if (right.containsKey(key)) {
				count++;
			}
		}
		System.out.println(count);

	}

}
