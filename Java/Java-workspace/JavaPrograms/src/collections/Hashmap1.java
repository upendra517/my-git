package collections;

import java.util.HashMap;
import java.util.Map;

public class Hashmap1 {

	public static void main(String[] args) {
		
		HashMap<Integer,String> hm = new HashMap<>();
		hm.put(1, "upendra");
		hm.put(2, "lava");
		hm.put(3, "naveen");
		System.out.println(hm);
		hm.replace(1, "upeee");
		System.out.println(hm);
		
		 for(Map.Entry<Integer,String> entry : hm.entrySet()){
			 System.out.println(entry.getKey() +" = " +entry.getValue());
		 }
		
	}

}
