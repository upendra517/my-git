package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Employee {
	
	@Id@GeneratedValue
	private int id;
	@Column(name = "empName")
	private String name;
	private double salary;
	private String city;

	public Employee() {
		
	}
	
	public Employee(int id,String name,double salary,String city){
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.city = city;
	}
	
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
