package com.example;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeDao employeeDao;
	
	@GetMapping("getAllEmployees")
	public List<Employee> getAllEmployees(){
		return employeeDao.getAllEmployees();
	}
	
	@PostMapping("addEmployee")
	public Employee addEmployee(@RequestBody Employee e){
		return employeeDao.addEmployee(e);
	}
	
	@PutMapping("updateEmployee/{id}")
	public Employee updateEmployee(@PathVariable int id,@RequestBody Employee e){
		Employee eee = new Employee(0,"Not found",0.0,"not found");
		Employee emp =  employeeDao.updateEmployee(id,e);
		if(emp != null){
			return emp;
		} else {
			return eee;
		}
	}
	
	// By declaring method return type as Object we can return any type of value like int,string,...;
	@DeleteMapping("deleteEmpById/{id}")
	public Object deleteEmployee(@PathVariable int id){
		employeeDao.deleteEmployee(id);
		return "employee with empid: " + id +" deleted";
	}
	
	@GetMapping("getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable int id){
		Employee eee = new Employee(0,"Not found",0.0,"not found");
		Employee emp =  employeeDao.getEmployeeById(id);
		if(emp != null){
			return emp;
		} else {
			return eee;
		}
	}
	
	@GetMapping("getEmployeeByName/{name}")
	public Employee getEmployeeByName(@PathVariable String name){
		return employeeDao.getEmpByName(name);
	}

}
