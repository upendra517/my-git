package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {

	@Autowired
	EmployeeRepository employeeRepository;

	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

	public Employee addEmployee(Employee e) {
		return employeeRepository.save(e);
	}

	public Employee updateEmployee(int id, Employee e) {

		Employee employee = employeeRepository.findById(id).orElse(null);

		if (employee != null) {

			employee.setName(e.getName());
			employee.setSalary(e.getSalary());

			return employeeRepository.save(employee);
		} else {
			return null;
		}
	}

	public void deleteEmployee(int id) {
		employeeRepository.deleteById(id);
	}

	public Employee getEmployeeById(int id) {
		Employee employee = employeeRepository.findById(id).orElse(null);

		if (employee != null) {

			return employee;
		} else {
			return null;
		}
	}
	
	public Employee getEmpByName(String name){
		return employeeRepository.findByName(name);
	}
}
