package com.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Employee;

@Service
public class EmployeeDao {
	
	@Autowired
	EmployeeRepository empRes;

	public Employee addEmployee(Employee emp) {
		return empRes.save(emp);
	}

	public List<Employee> getAllEmp() {
		return empRes.findAll();
	}

	public Employee deleteById(int id) {
		
		Employee emp = empRes.findById(id).orElse(null);
		
		if(emp != null){
			empRes.deleteById(id);
		}
		return emp;
		
	}

	public Employee updateEmp(int id, Employee emp) {
		Employee employee = empRes.findById(id).orElse(null);
		
		if(employee != null){
			employee.setName(emp.getName());
			employee.setDept(emp.getDept());
			employee.setEmail(emp.getEmail());
			employee.setPassword(emp.getPassword());
			employee.setSalary(emp.getSalary());
			
			empRes.save(employee);
		}
		return employee;
	}

	public Employee loginEmp(String email, String password) {
		Employee emp = empRes.empLogin(email, password);
		return emp;
	}

}
