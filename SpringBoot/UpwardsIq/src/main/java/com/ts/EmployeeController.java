package com.ts;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeDao employeeDao;
	
	@GetMapping("getall")
	public List<Employee> getAllEmp(){
		return employeeDao.getAllEmp();
	}
	
	@PostMapping("addEmp")
	public Employee addEmoployee(@RequestBody Employee emp){
		return employeeDao.addEmployee(emp);
	}
	
	
	@DeleteMapping("delete/{id}")
	public Employee deleteById(@PathVariable int id){
		return employeeDao.deleteById(id);
	}
	
	@PutMapping("updateEmp/{id}")
	public Employee updateEmp(@PathVariable int id, @RequestBody Employee emp){
		return employeeDao.updateEmp(id,emp);
	}
	
	@GetMapping("login/{email}/{password}")
	public Employee loginEmp(@PathVariable String email, @PathVariable String password){
		return employeeDao.loginEmp(email,password);
	}
	
	
}
