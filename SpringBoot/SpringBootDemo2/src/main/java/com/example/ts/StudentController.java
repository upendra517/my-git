package com.example.ts;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

//import com.dao.StudentDao;
import com.model.Student;

@CrossOrigin(origins="http://localhost:3000")
@RestController
public class StudentController {

	
     List<Student> students = new ArrayList<>();

	    public StudentController() {
	        students.add(s1);
	        students.add(s2);
	        students.add(s3);
	    }
	    
	    Student s1 = new Student(1, "upendra", "cse");
	    Student s3 = new Student(2, "upendra222", "cse");
	    Student s2 = new Student(3, "upendra33", "cse");
	
	@GetMapping("welcome")
	public String welcome(){
		return "welcome";
	}
	
	
	
	@GetMapping("getStudents")
	public List<Student> getStudent(){
		
		
		return  students;
	}
	
}
