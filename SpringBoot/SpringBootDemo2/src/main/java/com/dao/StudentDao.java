//package com.dao;
//
//import java.util.List;
//import java.util.Optional;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.web.bind.annotation.RequestBody;
//
//import com.model.Student;
//
//@Service
//public class StudentDao {
//	
//	@Autowired
//	StudentRepository studentRepository;
//	
//	public Student addStudent(Student s) {
//		return studentRepository.save(s);
//	}
//
//	public List<Student> getStudents() {
//		return studentRepository.findAll();
//	}
//
//	public Student updateStudent(int id,Student s) {
//		Student student = studentRepository.findById(id).orElse(null);
//		
//		if(s != null){
//			
//			if(s.getDept()!=null){
//			student.setDept(s.getDept());
//			}
//			if(s.getEmailId()!=null){
//			student.setEmailId(s.getEmailId());
//			}
//			if(s.getFee() != 0){
//			student.setFee(s.getFee());
//			}
//			if(s.getName() != null){
//			student.setName(s.getName());
//			}
//		}
//		return studentRepository.save(student);
//	}
//
//	public Student deleteStudent(int id) {
//		Student s = studentRepository.findById(id).orElse(null);
//		
//		studentRepository.deleteById(id);
//		return s;
//	}
//
//	public Student studentByEmailid(String emailId) {
//		
//		return studentRepository.findByEmailId(emailId);
//	}
//	
//}
