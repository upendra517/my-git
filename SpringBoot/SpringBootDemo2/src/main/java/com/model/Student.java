package com.model;

//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//
//@Entity
public class Student {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
//	private double fee;
	private String dept;
//	private String emailId;
//	private String password;
	
	public Student() {
		super();
	}
	
//	public Student(int id, String name, double fee, String dept, String emailId, String password) {
//		super();
//		this.id = id;
//		this.name = name;
//		this.fee = fee;
//		this.dept = dept;
//		this.emailId = emailId;
//		this.password = password;
//	}
	
	

	public Student(int id, String name, String dept) {
		super();
		this.id = id;
		this.name = name;
		this.dept = dept;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
//	public double getFee() {
//		return fee;
//	}
//	public void setFee(double fee) {
//		this.fee = fee;
//	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
//	public String getEmailId() {
//		return emailId;
//	}
//	public void setEmailId(String emailId) {
//		this.emailId = emailId;
//	}
//
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
	
	public String toString(){
		return id +" , "+ name +" , "+dept;
	}
	
}
